# Installer Apache
class lamp::apache {
  include apache

  file { '/var/www/html':
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0755',
    recurse => true,
  }

  file { '/var/www/html/index.html':
    ensure  => file,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0644',
    content => '<html><body><h1>Welcome to my LAMP stack!</h1></body></html>',
  }

  apache::vhost { 'localhost':
    port    => '80',
    docroot => '/var/www/html',
    options => ['Indexes', 'FollowSymLinks'],
  }
}

# Installer PHP
class lamp::php {
  include php

  package { 'php-mysql':
    ensure => present,
  }
}

# Installer MySQL
class lamp::mysql {
  include mysql::server

  mysql::db { 'mydatabase':
    ensure  => present,
    charset => 'utf8',
  }

  mysql::user { 'user@localhost':
    ensure     => absent,
    password   => mysql_password('password'),
    database   => 'database',
    privileges => ['all'],
  }
}

# Combinez les classes pour installer la stack LAMP complète
class lamp {
  include lamp::apache
  include lamp::php
  include lamp::mysql
}

# Appliquez la classe lamp à la machine cible
node 'CLRPL5' {
  include lamp
}
